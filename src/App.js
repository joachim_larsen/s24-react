import React, { Component } from 'react';
import './css/style.css';
import Header from './components/header';
import AddLogLine from './components/addLogLine';
import LogTable from './components/logTable';
import Footer from './components/footer';

class App extends Component {
  
  state = {
    pendingTankID: '',
    pendingAmount: '',
    totalAmount: 0,
    loglines: [],
    dayFinished: false,
  }
  
  handleTankIdInput = e =>
    this.setState({ pendingTankID: e.target.value });
    
  handleAmountInput = e =>
    this.setState({ pendingAmount: e.target.value });
  
  resetLogLineForm = () =>
    this.setState({
      pendingTankID: '',
      pendingAmount: '',
    });
  
  newLogLineSubmitHandler = e => {
    e.preventDefault();
    this.setState({
      loglines: [
        {
          tankID: parseInt(this.state.pendingTankID, 10),
          amount: parseInt(this.state.pendingAmount, 10),
          timeStamp: Date.now(),
        },
        ...this.state.loglines
      ],
      pendingTankID: '',
      pendingAmount: '',
      totalAmount: this.state.totalAmount + parseInt(this.state.pendingAmount, 10),
    });
  }
  
  removeLogLine = (index) => {
    const subtractAmount = this.state.loglines[index].amount;
    this.setState({
      loglines: [
        ...this.state.loglines.slice(0, index),
        ...this.state.loglines.slice(index + 1)
      ],
      totalAmount: this.state.totalAmount - subtractAmount,
    });
  }
    
  submitReport = () => {
    const text = "Er du ferdig for dagen og vil levere inn dagens rapport?";
    if(window.confirm(text) === true) {
      this.setState({
        dayFinished: true,
        pendingTankID: '',
        pendingAmount: '',
      });
      window.scrollTo(0, 0);
    }
  }
  
  resetApp = () =>
    this.setState({
      loglines: [],
      totalAmount: 0,
      dayFinished: false,
    })
  
  render() {
    return (
      <div>
        <Header />
        <main>
          <AddLogLine 
            pendingTankID={this.state.pendingTankID} 
            pendingAmount={this.state.pendingAmount}
            inputTankID={this.handleTankIdInput}
            inputAmount={this.handleAmountInput}
            resetLogLineForm={this.resetLogLineForm}
            submitLogLine={this.newLogLineSubmitHandler}
            dayFinished={this.state.dayFinished} />
          <LogTable 
            totalAmount={this.state.totalAmount}
            loglines={this.state.loglines}
            removeLogLine={this.removeLogLine}
            submitReport={this.submitReport} 
            pendingTankID={this.state.pendingTankID}
            pendingAmount={this.state.pendingAmount}
            dayFinished={this.state.dayFinished} 
            resetApp={this.resetApp} />
        </main>
        <Footer />
      </div>
    );
  }
}

export default App;
