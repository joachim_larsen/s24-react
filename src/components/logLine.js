import React from 'react';
import PropTypes from 'prop-types';

const LogLine = props => {
  const timeStamp = new Date(props.timeStamp);
  const time = timeStamp.getHours().toString().padStart(2, "0") + ':' + timeStamp.getMinutes().toString().padStart(2, "0");
  
  const button = props.dayFinished ? null : <button className="remove" onClick={props.handleRemove}>&#10007;</button>;
  
  return (
    <tr>
      <td>{props.tankID}</td>
      <td>{props.amount} m3</td>
      <td>kl. {time}</td>
      <td>{button}</td>
    </tr>
  );
};

LogLine.propTypes = {
  tankID: PropTypes.number.isRequired,
  amount: PropTypes.number.isRequired,
  timeStamp: PropTypes.number.isRequired,
  handleRemove: PropTypes.func.isRequired,
  dayFinished: PropTypes.bool.isRequired,
};

export default LogLine;