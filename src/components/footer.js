import React from 'react';

const Footer = () => {
  return (
    <footer>
      &copy; 2017 - Joachim Larsen
    </footer>
  );
};

export default Footer;