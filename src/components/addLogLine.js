import React from 'react';

const AddLogLine = props => {
  if (props.dayFinished) {
    return (
      <section className="dayFinished">
        <h2><span className="green">&#10003;</span> Ferdig!</h2>
        <p>Rapporten er mottatt. Takk for i dag! :-)</p>
      </section>
    );
  } else {
    return (
      <section className="addLogLine">
        <h2>Registrer tømming</h2>
        <form onSubmit={props.submitLogLine} onReset={props.resetLogLineForm}>
          <div className="fieldGroup">
            <label htmlFor="tankID">Seddelnummer:</label>
            <input 
              type="number"
              pattern="\d*"
              onChange={props.inputTankID}
              value={props.pendingTankID}
              id="tankID" 
              min="1000" 
              max="9999" 
              placeholder="&bull; &bull; &bull; &bull;" 
              required />
          </div>
          <div className="fieldGroup">
            <label htmlFor="amount">Tømt (m3):</label>
            <input
              type="number"
              pattern="\d*"
              onChange={props.inputAmount}
              value={props.pendingAmount}
              id="amount"
              min="1"
              max="100"
              required />
          </div>
          <div className="buttonGroup">
            <button type="reset"><span className="red">&#10007;</span> Nullstill</button>
            <button type="submit"><span className="green">&#10003;</span> Lagre</button>
          </div>
        </form>
      </section>
    );
  }
};

export default AddLogLine;