import React from 'react';
import PropTypes from 'prop-types';
import LogLine from './logLine';
import PendingLogLine from './pendingLogLine';

const LogTable = props => {
  let button;
  if (props.dayFinished) {
    button = <button className="bottomButton" onClick={props.resetApp}>Start på nytt</button>
  } else {
    button = <button className="bottomButton" onClick={props.submitReport}>Ferdig for dagen</button>
  }

  if (props.loglines.length === 0 && !props.pendingTankID && !props.pendingAmount) {
    return (
      <section className="logTable">
        <p className="grayText"><em>Du har ennå ikke registrert noe aktivitet for i dag...</em></p>
      </section>
    );
  } else {
    return (
      <section className="logTable">
        <div className="tableHeader">
          <h2>Dagens logg</h2>
          <h2 className="totalAmount"><small>totalt</small> {props.totalAmount} <small>m3</small></h2> 
        </div>
        <table>
          <thead>
            <tr>
              <th>Seddelnr.</th>
              <th>Tømt</th>
              <th>Tid</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <PendingLogLine 
              tankID={props.pendingTankID}
              amount={props.pendingAmount} />
            {props.loglines.map((logline, index) =>
              <LogLine
                key={index}
                tankID={logline.tankID}
                amount={logline.amount}
                timeStamp={logline.timeStamp}
                handleRemove={() => props.removeLogLine(index)} 
                dayFinished={props.dayFinished} />
            )}
          </tbody>
        </table>
        {button}
      </section>
    );
  }
};

LogTable.propTypes = {
  loglines: PropTypes.array.isRequired,
  removeLogLine: PropTypes.func.isRequired,
  submitReport: PropTypes.func.isRequired,
  pendingTankID: PropTypes.string.isRequired,
  pendingAmount: PropTypes.string.isRequired,
  dayFinished: PropTypes.bool.isRequired,
};

export default LogTable;