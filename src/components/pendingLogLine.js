import React from 'react';
import PropTypes from 'prop-types';

const PendingLogLine = props => {
  
  if (props.tankID || props.amount) {
    const m3 = props.amount ? "m3" : null;
    
    return (
      <tr className="pending">
        <td>{props.tankID}</td>
        <td>{props.amount} {m3}</td>
        <td></td>
        <td></td>
      </tr>
    );
  }
  return null;
};


PendingLogLine.propTypes = {
  tankID: PropTypes.string.isRequired,
  amount: PropTypes.string.isRequired,
};

export default PendingLogLine;