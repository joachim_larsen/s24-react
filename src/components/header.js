import React from 'react';
import menuIcon from '../menu_icon.png';

const Header = () => {
  return (
    <header>
      <h1>Septik24</h1>
      <img src={menuIcon} width="35" height="25" alt="menu icon" />
    </header>
  );
};

export default Header;